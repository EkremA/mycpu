module decoder_bcd_onehot_basic_tb();
	parameter bcd_width = 4;
	parameter onehot_width = 10;

	reg [bcd_width-1:0] data_in;
	reg [onehot_width-1:0] data_out;

	decoder_bcd_onehot decoder_bcd_onehot_I (.a(data_in), .y_n(data_out));

	initial begin
		forever begin
			data_in = 1;
			#10
			data_in = 3;
			#10
			data_in = 14;
			#10
			data_in = 0;
			#10
			$finish;
		end 
	end
endmodule
