module full_adder_basic_tb();
	parameter operand_width = 4;
	parameter result_width = operand_width;
	
	reg [operand_width-1:0] data_in1;
	reg [operand_width-1:0] data_in2;
	reg [result_width-1:0] data_out;
	reg data_carry;
	
	full_adder #(.operand_width(operand_width), .result_width(result_width)) full_adder_I (.a(data_in1), 
																							.b(data_in2), 
																							.sum(data_out), 
																							.carry_in(0),
																							.carry_out(data_carry));

	initial begin
		forever begin
			data_in1 = 4;
			data_in2 = 5;
			#5
			data_in1 = 14;
			data_in2 = 2;
			#5
			data_in1 = 14;
			data_in2 = 5;
			#5
			$finish;
		end 
	end
endmodule
