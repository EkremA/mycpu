module tristate_basic_tb();

	reg data_in;
	reg data_out;
	reg ctrl;

	tristate tristate_I (.x(data_in), .y(data_out), .g_n(ctrl));

	initial begin
		forever begin
			data_in = 1'b1;
			ctrl = 1'b0;
			#10
			ctrl = 1'b1;;
			#10
			data_in = 1'b0;
			#10
			$finish;
		end 
	end
endmodule
