module full_adder_multi_tb();
	parameter inp_per_fa_width = 4;
	parameter res_per_fa_width = inp_per_fa_width;

	parameter operand_width = 2*inp_per_fa_width;
	parameter result_width = operand_width;

	reg zero = 0;
	reg [operand_width-1:0] data_in1;
	reg [operand_width-1:0] data_in2;
	reg [operand_width-1:0] data_out;
	
	wire inter_carry;
	reg data_carry;

	full_adder #(.operand_width(inp_per_fa_width), 
					.result_width(res_per_fa_width)) full_adder_1_I (.a(data_in1[inp_per_fa_width-1:0]), 
																	.b(data_in2[inp_per_fa_width-1:0]), 
																	.sum(data_out[inp_per_fa_width-1:0]), 
																	.carry_in(zero),
																	.carry_out(inter_carry));
	full_adder #(.operand_width(inp_per_fa_width), 
					.result_width(res_per_fa_width)) full_adder_2_I (.a(data_in1[operand_width-1:inp_per_fa_width]), 
																	.b(data_in2[operand_width-1:inp_per_fa_width]), 
																	.sum(data_out[operand_width-1:inp_per_fa_width]), 
																	.carry_in(inter_carry),
																	.carry_out(data_carry));


	initial begin
		forever begin
			data_in1 = 4;
			data_in2 = 5;
			#5
			data_in1 = 14;
			data_in2 = 2;
			#5
			data_in1 = 14;
			data_in2 = 5;
			#5
			data_in1 = 110;
			data_in2 = 120;
			#5
			data_in1 = 125;
			data_in2 = 130;
			#5
			data_in1 = 255;
			data_in2 = 1;
			#5
			data_in1 = 220;
			data_in2 = 200;
			#5
			data_in1 = 0;
			data_in2 = 0;
			#5
			$finish;
		end 
	end
endmodule
