module and_basic_tb();
	parameter num_inp_per_and = 2;
	parameter num_and = 4;
	parameter num_inp_bits = num_inp_per_and * num_and;
	
	reg [num_inp_bits-1:0] data_in;
	reg [num_and-1:0] data_out;
	
	mos4081 #(.inputs_per_gate(num_inp_per_and), .n_gates(num_and)) and_ic_I (.x(data_in), .y(data_out));

	initial begin
		forever begin
			data_in = {num_inp_bits{1'b1}};
			#10
			data_in = {{num_inp_bits-1{1'b1}}, {1{1'b0}}};
			#10
			$finish;
		end 
	end
endmodule
