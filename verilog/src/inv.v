module inverter #(PARAMETER n=6) (input wire [n-1:0] x, output wire [n-1] y);

	integer i;
	always @(x) begin
		y~=x;
	end
endmodule