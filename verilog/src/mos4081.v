module mos4081 #(parameter inputs_per_gate=2, n_gates=4) (input wire [(inputs_per_gate*n_gates)-1:0] x, 
															output wire [n_gates-1:0] y);

	//MOS4081
	//--4*ANDs with 2 inps--	
	genvar i;
	generate
		for (i = 0; i < n_gates; i=i+1) begin
				assign y[i] = x[2*i] & x[2*i+1];
		end
	endgenerate

endmodule