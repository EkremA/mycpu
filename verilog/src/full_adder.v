module full_adder #(parameter operand_width = 4, result_width = 4) (input wire [operand_width-1:0] a,
																	input wire [operand_width-1:0] b,
																	output reg [result_width-1:0] sum,
																	input wire carry_in,
																	output reg carry_out);
	reg carry_temp;
	reg [1:0] sum_temp;

	integer i;
	always @* begin
		//initialise helper vars
		carry_temp = carry_in;
		sum_temp = 2'b00;
		//let's get cracking
			for (i=0; i<result_width; i=i+1) begin
				sum_temp = a[i] + b[i] + carry_temp;
				sum[i] = sum_temp[0];
				carry_temp = sum_temp[1]; 
			end
		carry_out = carry_temp;
	end
endmodule
