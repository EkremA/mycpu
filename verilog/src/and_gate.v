module and_gate #(parameter inputs_per_gate=2) (input wire [inputs_per_gate-1:0] x, 
															output wire y);	
	
	genvar i;
	generate
		assign y = x[0];
		for (i = 1; i < inputs_per_gate; i=i+1) begin
				assign y = y & x[i];
		end
	endgenerate
endmodule