module inverter #(PARAMETER n=6) (input wire [n-1:0] x, output wire [n-1] y);
//MOS4069
	integer i;
	always @(x) begin
		y~=x;
	end
endmodule