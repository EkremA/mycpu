module tristate (input wire x, 
				output reg y,
				input wire g_n);
	//what happens if i do not specify wire or reg or so=
	always @(g_n) begin
		casex(g_n)
			1'b1 : y = 1'bZ;
			1'b0 : y = x;
			default : y = 1'bX;
		endcase
	end
endmodule