module #(PARAMETER s_length=4, y_length=10) decoder_b2onehot (input wire [s_length-1:0] binary_in,
													output wire [y_length-1:0] onehot_out_n);
//-------------------
//		74HC42
//-------------------
// y_length > s_length
//Has an inverted output 
	integer i;
	always @(binary_in) begin
		for (i=0; i<y_length; i=i+1) begin 
			if (i==binary_in) onehot_out_n=1'b0;
			else onehot_out_n=1'b1; 
		end
	end
endmodule