module decoder_bcd_onehot #(parameter bcd_width = 4, onehot_width = 10) (input wire [bcd_width-1:0] a, 
																		output reg [onehot_width-1:0] y_n);
	always @(a) begin 
		y_n = {onehot_width{1'b1}};
		if (a < 10)
			y_n[a] = 1'b0;
	end
endmodule