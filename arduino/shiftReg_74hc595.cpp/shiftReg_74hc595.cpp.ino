//My 32 Bit Shiftreg Arduino Script to set and read out data in different interfaces in my project
#define SI 53
#define G_n 50
#define RCK 49
#define SCK 46
#define SCLR_n 45

void setup() {
  // put your setup code here, to run once:
  pinMode(SI, OUTPUT);
  pinMode(G_n, OUTPUT);
  pinMode(RCK, OUTPUT);
  pinMode(SCK, OUTPUT);
  pinMode(SCLR_n, OUTPUT);



  digitalWrite(SI, LOW);  //start serial in as 0
  digitalWrite(G_n, LOW); //outputs enabled
  digitalWrite(RCK, LOW); //nothing pushed onto output
  digitalWrite(SCK, LOW); //shiftin clock init as 0
  digitalWrite(SCLR_n, HIGH); //Init with no claring of shiftregs

  Serial.begin(9600);
}

/*void toggleOutput()
{
  //... change G_n
  auto state = digitalRead(G_n);
  Serial.println("init");
  Serial.println(state);
  if(state==0)
    digitalWrite(G_n, HIGH);
  else if(state==1)
    digitalWrite(G_n, LOW);
 }

void updateOutput()
{
  digitalWrite(RCK, LOW);
  report();
  delay(100);
  digitalWrite(RCK, HIGH);
  report();
  delay(100);
  digitalWrite(RCK, LOW);
  report();
}

void clear_74hc595()
{
  //...SCLR use to clear shift regs
  digitalWrite(SCLR_n, LOW);
  digitalWrite(SCLR_n, HIGH);
}
*/
void shiftInBit_74hc595(int bitValue) 
{
  //shiftIn either a 1 or a 0, set param accordingly
  digitalWrite(RCK, LOW);
  if (bitValue==0)
    digitalWrite(SI, LOW);
  else
    digitalWrite(SI, HIGH);
  digitalWrite(SCK, HIGH);
  digitalWrite(SCK, LOW);
  digitalWrite(RCK, HIGH);
  
}

void shiftIn_74hc595(long decNum32Bit, char bitOrder, double freq) 
{
    /* 
    Shifts in a 32 Bit representation of a number into a cascade of four 74hc595 shiftregisters.
  
    Args:
      decNum32Bit: decimal representation of the number to be shifted in
      bitOrder: Declares order by which to shift in the bits. (values: "MSBFIRST" | "LSBFIRST", default "MSBFIRST")
      freq: Frequency at which a new number is fully shifted in (in Hz), maximal value of 10^6 Hz.
    */  

    int myBits [32];
    int decTemp = decNum32Bit;
    //get individual bits
    for(int i=0; i<32; i++)
    {
      myBits[i] = decTemp % 2;
      decTemp = decTemp / 2;
    }
    //i=0 is LSB, i=31 is MSB

    if (bitOrder=="MSBFIRST"){
      for(int i=31; i>=0; i--)
        shiftInBit_74hc595(myBits[i]);
    }
    else if (bitOrder=="LSBFIRST")
    {
      for(int i=0; i<32; i++)
        shiftInBit_74hc595(myBits[i]);
    }
    else
    {
      //This is MSBFIRST in any other case
      for(int i=31; i>=0; i--)
        shiftInBit_74hc595(myBits[i]);
      //toggleOutput()
    }

    //TODO: Put this into setup()
    if (1.0/freq < 1.0)
      delayMicroseconds(1); //us
    else
      delayMicroseconds(1.0/freq);

}


void report()
{
   Serial.print("G_n: ");
   Serial.print(digitalRead(G_n));
   Serial.print("\n");
   
   Serial.print("RCK: ");
   Serial.print(digitalRead(RCK));
   Serial.print("\n");

   Serial.print("SCK: ");
   Serial.print(digitalRead(SCK));
   Serial.print("\n");

   Serial.print("SCLR_n: ");
   Serial.print(digitalRead(SCLR_n));
   Serial.print("\n");
   
}

//Proper Hardware Interface to choose everything (maybe with own menu and display and everything)

int i = 1;
void loop() {
  Serial.println("Next ITER\n################");
  i+=1;
  shiftIn_74hc595(bitOrder="MSBFIRST");
  delay(1000);
}
